<?php
return [
    'production' => false,
    'baseUrl' => '',
    'language' => 'de',
    'title' => 'Nikus Blog',
    'owner' => 'Niku',
    'email' => 'publicmail [at] [thisDomain] [dot] de',
    'description' => 'Blogs über IT-Stuff, meine Projekte und allem was sonst so anfällt.',
    'locale' => 'de_DE',
    'dateFormat' => 'd. MMMM yyyy',
    'collections' => [
        'posts' => [
            'excerpt' => function ($page, $length = 75) {
                if (count(explode(' ', strip_tags($page->getContent()))) <= $length){
                    return strip_tags($page->getContent());
                } else {
                    return implode(' ', array_slice(explode(' ', strip_tags($page->getContent())), 0, $length)) . '...';
                }
            }
        ]
    ],
    'dateTranslated' => function ($page, $date) {
        $format = new IntlDateFormatter(
            $page->locale,
            IntlDateFormatter::NONE,
            IntlDateFormatter::NONE,
            Null,
            Null,
            $page->dateFormat);
        return datefmt_format($format, $date);
    }
];
