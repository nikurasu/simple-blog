---
pagination:
    collection: posts
    perPage: 5
---

@extends('_layouts.main')

@section('body')
@foreach($pagination->items as $post)
    <article>
        <section>
            <h2><a href="{{ $post->getUrl() }}">{{ $post->title }}</a></h2><small>{{ $page->dateTranslated($post->date) }}</small>
        </section>
        <section>
            <p>
                {{ $post->excerpt() }}
            </p>
        </section>
    </article>
    <hr>
@endforeach
<section class="paginationNavigation">
    @if ($previous = $pagination->previous)
        <a href="{{ $page->baseUrl }}{{ $pagination->first }}">&lt;&lt;</a>&nbsp;
        <a href="{{ $page->baseUrl }}{{ $previous }}">&lt;</a>&nbsp;
    @else
        &lt;&lt;&nbsp;&lt;&nbsp;
    @endif

    @foreach ($pagination->pages as $pageNumber => $path)
        @if($pageNumber !== 1)&nbsp;@endif
            <a href="{{ $page->baseUrl }}{{ $path }}"
           class="{{ $pagination->currentPage == $pageNumber ? 'selected' : '' }}">{{ $pageNumber }}</a>
    @endforeach

    @if ($next = $pagination->next)
            &nbsp;<a href="{{ $page->baseUrl }}{{ $next }}">&gt;</a>&nbsp;
        <a href="{{ $page->baseUrl }}{{ $pagination->last }}">&gt;&gt;</a>
    @else
            &nbsp;&gt;&nbsp;&gt;&gt;
    @endif
</section>

@endsection
