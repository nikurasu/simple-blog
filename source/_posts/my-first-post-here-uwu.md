---
extends: _layouts.post
title: My first post here UwU
date: 2017-03-23
---
So I have completely rewritten my blog page. Why you may ask? Because I discovered the static site generator jigsaw,
wich does a far better job then jekyll, at least in my opinion. Anything here is made without any templates from scratch,
without any JavaScript bloat. Enjoy your stay!