<!DOCTYPE html>
<html lang="{{ $page->language ?? 'en' }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="canonical" href="{{ $page->getUrl() }}">
        <meta name="description" content="{{ $page->description }}">
        <meta name="color-scheme" content="dark light">
        <title>{{ $page->owner }}s Blog</title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
    </head>
    <body>
    <header>
        <h1>{{ $page->owner }}s Blog</h1>
        <nav>
            <ul>
                <li @if($page->getPath() === '/')  aria-current="page" @endif><a href="/">Home</a></li>
                <li @if($page->getPath() === '/projects')  aria-current="page" @endif><a href="/projects">Projects</a></li>
                <li @if($page->getPath() === '/about')  aria-current="page" @endif><a href="/about">About</a></li>
            </ul>
        </nav>
    </header>
    <main>
        @yield('body')
    </main>
    <footer>
        <div>
            <section>
                <p>{{ $page->description }}</p>
                <p>Alle Shitposts hier sind unter der <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a>
                lizensiert, außer sie sind anders gekennzeichent.</p>
                <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
                <br /><br />
                <img alt="Always wear your programming socks and thigh highs bedside" src="{{ '/assets/images/programmingsocks.gif' }}">
                <img alt="An enby flag with the text: Maintained by a queer folk" src="{{ '/assets/images/nocookie.gif' }}">
                <img alt="This site is certified 100% Cookie free" src="{{ '/assets/images/enby.png' }}">
                <a href="https://validator.w3.org/nu/?doc=https%3A%2F%2Fblog.nikurasu.de"><img alt="W3C HTML5 badge" src="{{ '/assets/images/html5.png' }}"></a>
                <img alt="Pride and a LGBTQ flag" src="{{ '/assets/images/pride.gif' }}">
                <img alt="Not tracked" src="{{ '/assets/images/nottracked.png' }}">
            </section>
            <section>
                <p>Linkpage: <a href="https://nikurasu.de">https://nikurasu.de</a></p>
                <p>E-Mail: <a href="mailTo({{ $page->email }})">{{ $page->email }}</p>
                <p><a href="/rss.xml">RSS-Feed</a></p>
                <p><a href="#">Proof of Identity</a></p>
            </section>
        </div>
    </footer>
    </body>
</html>
