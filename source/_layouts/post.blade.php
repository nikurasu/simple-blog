@extends('_layouts.main')

@section('body')
    <article>
        <section>
            <h2>{{ $page->title }}</h2>
            <small>{{ $page->dateTranslated($page->date)  }}</small>
        </section>
        <section>
            @yield('content')
        </section>
    </article>
@endsection