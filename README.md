# Nikus Blog Template

Here is the template of my blog, released under the MIT license.

Format Date for Posts like here
https://unicode-org.github.io/icu/userguide/format_parse/datetime/#date-field-symbol-table

If you want to use the post for yourself, create own posts in `source/_posts` and edit the content of `source/about.blade.php` and `source/projects.blade.php`. To change the name of the pages, you also have to edit the navbar in the `source/_layouts/main.blade.php` to your choosen names.